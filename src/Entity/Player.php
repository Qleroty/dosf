<?php

namespace App\Entity;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idPlayer;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $namePlayer;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nameKingdom;

    /**
     * @ORM\Column(type="integer")
     */
    private $statAttack;

    /**
     * @ORM\Column(type="integer")
     */
    private $statDefense;

    /**
     * @ORM\Column(type="integer")
     */
    private $statSpeed;

    /**
     * @ORM\Column(type="integer")
     */
    private $statHp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPlayer(): ?int
    {
        return $this->idPlayer;
    }

    public function setIdPlayer(int $idPlayer): self
    {
        $this->idPlayer = $idPlayer;

        return $this;
    }

    public function getNamePlayer(): ?string
    {
        return $this->namePlayer;
    }

    public function setNamePlayer(string $namePlayer): self
    {
        $this->namePlayer = $namePlayer;

        return $this;
    }

    public function getNameKingdom(): ?string
    {
        return $this->nameKingdom;
    }

    public function setNameKingdom(string $nameKingdom): self
    {
        $this->nameKingdom = $nameKingdom;

        return $this;
    }

    public function getStatAttack(): ?int
    {
        return $this->statAttack;
    }

    public function setStatAttack(int $statAttack): self
    {
        $this->statAttack = $statAttack;

        return $this;
    }

    public function getStatDefense(): ?int
    {
        return $this->statDefense;
    }

    public function setStatDefense(int $statDefense): self
    {
        $this->statDefense = $statDefense;

        return $this;
    }

    public function getStatSpeed(): ?int
    {
        return $this->statSpeed;
    }

    public function setStatSpeed(int $statSpeed): self
    {
        $this->statSpeed = $statSpeed;

        return $this;
    }

    public function getStatHp(): ?int
    {
        return $this->statHp;
    }

    public function setStatHp(int $statHp): self
    {
        $this->statHp = $statHp;

        return $this;
    }
}
